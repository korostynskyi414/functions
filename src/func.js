const getSum = (str1, str2) => {
  if(typeof(str1) !== 'string' || typeof(str2) !== 'string') return false;
  if(str1 == '') str1 = '0';
  if(str2 == '') str2 = '0';
  if(isNaN(str1) || isNaN(str2)) return false;
  var res = '';
  if(str1.length > str2.length) str2 = '0'.repeat(str1.length - str2.length) + str2; else str1 = '0'.repeat(str2.length - str1.length) + str1;
  var over = false;
  for(let i = str1.length - 1; i >= 0; i--){
    let addition = parseInt(str1.charAt(i)) + parseInt(str2.charAt(i));
    if(over) addition++;
    if(addition > 9){
      addition = addition % 10;
      over = true;
    } else over = false;
    res = addition.toString() + res;
  }
  if(over) res = '1' + res;
  return res;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var posts = 0;
  var comments = 0;
  for(let l of listOfPosts){
    if(l.author == authorName) posts++;
    if(l.hasOwnProperty('comments')) for(let c of l.comments) if(c.author == authorName) comments++;
  }
  return `Post:${posts},comments:${comments}`;
};

const tickets=(people)=> {
  let money = {
    25: 0,
    50: 0
  }
  for(let p of people){
    let val = typeof(p) == 'string' ? parseInt(p) : p;
    if(val == 25){ 
      money[25]++;
      continue;
    }
    val -= 25;
    if(val > 50){
       if(money[25] == 0 || (money[50] == 0 && money[25] < 3)) return 'NO';
       else {
        if(money[50] == 0) money[25] -= 3; else {
          money[50]--;
          money[25]--;
        }
       }
    }
    else {
      if(money[25] == 0) return 'NO';
      else {
        money[25]--;
        money[50]++;
      }
    } 
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
